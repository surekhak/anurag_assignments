CREATE PROCEDURE encryptFunction
(
@plainText  nvarchar(100)
)
AS
BEGIN
DECLARE @encryptedText NVARCHAR(100)
DECLARE @index INT
SET @index = 1
SET @encryptedText = ''
WHILE @index <= LEN(@plainText)
BEGIN
SET @encryptedText = @encryptedText + NCHAR(ASCII( SUBSTRING(@plainText, @index, 1)) ^129)
SET @index = @index + 1
END
SELECT @encryptedText AS ENCRYPTED_TEXT
END