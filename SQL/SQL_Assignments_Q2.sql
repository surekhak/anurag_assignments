create procedure CSVtoROWS
  @Str varchar(50)
as

declare @tbl_row table(string varchar(50))

while len(@Str) > 0
begin
  insert into @tbl_row values(left(@Str, charindex(',', @Str+',')-1))
  set @Str = stuff(@Str, 1, charindex(',', @Str+','), '')
end

select string from @tbl_row