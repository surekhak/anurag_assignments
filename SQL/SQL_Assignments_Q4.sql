USE [AssignmentDatabase]
GO
/****** Object:  StoredProcedure [dbo].[rankingProcedure]    Script Date: 2/6/2017 10:38:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[rankingProcedure]
( 
	@district_name VARCHAR(100),
    @rank INT
)
AS
BEGIN
with cte_exp as
(
SELECT district_id, district_name, population,
RANK() OVER (ORDER BY population desc) AS Ranking
FROM tbl_district
where district_name = @district_name
)select  district_name, population from cte_exp where Ranking = @rank 
END