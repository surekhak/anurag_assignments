CREATE PROCEDURE data_Encryption (@input_text VARCHAR(50), @keyvalue VARCHAR(8))
AS
BEGIN
DECLARE @encrypt_text VARBINARY(50)
DECLARE @i INT
DECLARE @text VARCHAR(50)

SET @i=LEN(@keyvalue)

IF @i>=6 and @i<=8
	BEGIN

	SET @text=(SELECT plain_text from tbl_SQL_10 WHERE key_value=@keyvalue and encrypted_text=@input_text)
	IF @text=@input_text
	BEGIN
		SELECT encrypted_text from tbl_SQL_10 WHERE key_value=@keyvalue and plain_text=@input_text
	END

	SET @text=(Select encrypted_text from tbl_SQL_10 where key_value=@keyvalue and plain_text=@input_text)
	IF @text=@input_text
	BEGIN
		SELECT plain_text from tbl_SQL_10 WHERE key_value=@keyvalue and encrypted_text=@input_text
	END

		SET @encrypt_text=ENCRYPTBYPASSPHRASE(@keyvalue,@input_text)
		Insert into tbl_SQL_10 values (@input_text,@keyvalue,@encrypt_text)
		SELECT * FROM tbl_SQL_10
	END
	else
		select 'Key value must be between 6 to 8 character' as ERROR
END