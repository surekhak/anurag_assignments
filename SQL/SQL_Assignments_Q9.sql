-- ================================================
-- Template generated from Template Explorer using:
-- Create Scalar Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION replace_alphabtes
(
	@str VARCHAR(50)
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @i int
	DECLARE @temp int
	DECLARE @ch CHAR
	DECLARE @reverse_str VARCHAR(50)

	SET @i=1
	SET @reverse_str=''

	WHILE @i<=LEN(@str)
	BEGIN

		SET @ch=SUBSTRING(@str,@i,1)

		IF ASCII(@ch)>=ASCII('A') and ASCII(@ch)<=ASCII('Z')
		BEGIN
		SET @temp=(ASCII(@ch)+25)-((ASCII(@ch)-65)*2)
		SET @reverse_str=CONCAT(@reverse_str,CHAR(@temp))
		END
		ELSE

		IF ASCII(@ch)>=ASCII('a') and ASCII(@ch)<=ASCII('z')
		BEGIN
		SET @temp=(ASCII(@ch)+25)-((ASCII(@ch)-97)*2)
		SET @reverse_str=CONCAT(@reverse_str,CHAR(@temp))
		END
		ELSE

		IF ASCII(@ch)>=ASCII('0') and ASCII(@ch)<=ASCII('9')
		BEGIN
		SET @temp=(ASCII(@ch)+9)-((ASCII(@ch)-48)*2)
		SET @reverse_str=CONCAT(@reverse_str,CHAR(@temp))
		END
		ELSE

		IF ASCII(@ch)<128
		BEGIN
		SET @temp=ASCII(@ch)+128
		SET @reverse_str=CONCAT(@reverse_str,CHAR(@temp))
		END
		ELSE

		IF ASCII(@ch)>128
		BEGIN
		SET @temp=ASCII(@ch)-128
		SET @reverse_str=CONCAT(@reverse_str,CHAR(@temp))
		END

		SET @i=@i+1;
	
	END
	RETURN @reverse_str
END