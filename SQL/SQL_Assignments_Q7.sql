-- ================================================
-- Template generated from Template Explorer using:
-- Create Multi-Statement Function (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION getMonths1 
(
	@date date
	)
RETURNS 
@tbl_m TABLE 
(
	months varchar(50)
)
AS
BEGIN
	DECLARE @month INT;
DECLARE @i INT;
DECLARE @month_list VARCHAR(200);

SET @i=1;
SET @month_list='';
SET @month=DATEPART(MM,@date);
 
 while @i<=@month
 BEGIN
 insert into @tbl_m (months) SELECT month_name from calender where id=@i;
 SET @i=@i+1;
 END;
	RETURN
END
GO