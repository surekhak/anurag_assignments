USE [AssignmentDatabase]
GO
/****** Object:  StoredProcedure [dbo].[distCount]    Script Date: 2/6/2017 10:47:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[distCount]
(
	@num INT
)
AS
with cte_exp AS
(
SELECT district_name, COUNT(district_name) AS con FROM tbl_district
group by district_name
)
select district_name from cte_exp WHERE con=@num