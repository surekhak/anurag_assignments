USE [AssignmentDatabase]
GO
/****** Object:  UserDefinedFunction [dbo].[phoneNoVal1]    Script Date: 2/6/2017 10:30:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER function [dbo].[phoneNoVal1]
(
@phone_no varchar(10)
)
RETURNS VARCHAR(50)
BEGIN

declare @phone_length int;
declare @expression varchar(20);
declare @lastno varchar(4);
declare @leftno varchar(3);

set @phone_length=len(@phone_no);
set @expression ='[^a-zA-Z0-9]';

if @phone_length=10
 begin
	set @phone_no=REPLACE(@phone_no,'A',2);
    set @phone_no=REPLACE(@phone_no,'B',2);
    set @phone_no=REPLACE(@phone_no,'C',2);
    set @phone_no=REPLACE(@phone_no,'D',3);
    set @phone_no=REPLACE(@phone_no,'E',3);
    set @phone_no=REPLACE(@phone_no,'F',3);
    set @phone_no=REPLACE(@phone_no,'G',4);
    set @phone_no=REPLACE(@phone_no,'H',4);
    set @phone_no=REPLACE(@phone_no,'I',4);
    set @phone_no=REPLACE(@phone_no,'J',5);
    set @phone_no=REPLACE(@phone_no,'K',5);
    set @phone_no=REPLACE(@phone_no,'L',5);
    set @phone_no=REPLACE(@phone_no,'M',6);
    set @phone_no=REPLACE(@phone_no,'N',6);
    set @phone_no=REPLACE(@phone_no,'O',6);
    set @phone_no=REPLACE(@phone_no,'P',7);
    set @phone_no=REPLACE(@phone_no,'Q',7);
    set @phone_no=REPLACE(@phone_no,'R',7);
    set @phone_no=REPLACE(@phone_no,'S',8);
    set @phone_no=REPLACE(@phone_no,'T',8);
    set @phone_no=REPLACE(@phone_no,'U',8);
    set @phone_no=REPLACE(@phone_no,'V',9);
    set @phone_no=REPLACE(@phone_no,'W',9);
    set @phone_no=REPLACE(@phone_no,'X',9);
    set @phone_no=REPLACE(@phone_no,'Y',9);
    set @phone_no=REPLACE(@phone_no,'Z',9);
	set @phone_no=REPLACE(@phone_no,1,1);
	set @phone_no=REPLACE(@phone_no,0,0);
    
	set @lastno=RIGHT(@phone_no,4);

	set @phone_no=CONCAT('(',SUBSTRING(@phone_no,1,3),')',' ',SUBSTRING(@phone_no,4,3),'-',SUBSTRING(@phone_no,5,4));

	return CONCAT(@phone_no,'-',@lastno);

	end;

    return'Invalid Format';
    
    
END
