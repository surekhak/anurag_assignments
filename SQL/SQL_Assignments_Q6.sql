USE [AssignmentDatabase]
GO
/****** Object:  StoredProcedure [dbo].[EvenOdd_District]    Script Date: 2/4/2017 4:04:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[EvenOdd_District]
AS
BEGIN

      SELECT d.district_id ,d.district_name,d1.district_id ,d1.district_name
      FROM tbl_district d,tbl_district d1 WHERE d.district_id=
         CASE
            when (d.district_id%2)!=0 then d.district_id 
         END
         AND d1.district_id=d.district_id+1;
END