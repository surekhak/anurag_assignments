CREATE function SeparatePopulation1( @dist_name varchar(20))

returns varchar(500)

BEGIN
  
  declare @i int;
  declare @cnt int;
  declare @tbl table (pop int,Id int NOT NULL IDENTITY(1,1))
  declare @output varchar(500);
  
  set @i=0;
  set @output='';
  select @cnt = count(district_name) from tbl_district where district_name=@dist_name;
   while @i<@cnt begin
     
	 insert into @tbl (pop) select population from tbl_district where district_name=@dist_name
	 SET @output=CONCAT(@output,(select pop from @tbl where id=@i),',')
    set @i=@i+1;
    end;
     
   return @output;
end;