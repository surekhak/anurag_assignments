function validate(){
		var fname=document.getElementById("firstname").value;
		var lname=document.getElementById("lastname").value;
		var uname=document.getElementById("username").value
		var dob=document.getElementById("DOB").value;
		var email=document.getElementById("email").value;
		var phone=document.getElementById("pnumber").value;
				
		var name_regex = /^[a-zA-Z]+$/;
		var email_regex = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
		var msg="";
		
		if(fname==""||lname==""||dob==""||phone==""||email==""){
			msg+="All feilds are required"+"\n";
		}
		if(!fname.match(name_regex)){
			msg+="Please use only alphabates first name"+"\n";
		}
		if(!lname.match(name_regex)){
			msg+="Please use only alphabates last name"+"\n";
		}
		
		if(!(uname.length>= 6 && uname.length <= 8)){
			msg+="Please enter username between 6 and 8 characters"+"\n";
		}
		
		if(!email.match(email_regex))
		{
			msg+="Please enter a valid email address"+"\n"
		}
		
		if(dob==""){
			msg+="Please fill Date of Birth"+"\n";
		}
		
		if(phone.length!=10||phone.match(name_regex)){
			msg+="Please enter valid phone number"+"\n";
		}
		
		if(msg!=""){
			alert(msg);
		}
		
	}
	